<?php
session_start();
include_once "controladores/AdminControl.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Isima</title>
    <link rel="stylesheet" href="dist/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/style.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

</head>
<body>

<?php
if (isset($_POST['submit'])) {
  $email = $_POST['loginEmail'];
  $password = $_POST['loginPassword'];

  $admin = new AdminControl();
  $result = $admin->login($email, $password);
}
if (isset($_SESSION["user"])) {
  header("Location: http://".$_SERVER['HTTP_HOST']."/Productos.php");
}
?>
<style media="screen">
  body {
    background-color: #F9F9F9;
  }

  .container{
    margin-top: 5%;
  }

  .card-header {
        padding: 5px 15px;
  }

    .profile-img {
        width: 96px;
    height: 96px;
        margin: 0 auto 10px;
        display: block;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
    }
</style>
<div class="">
	<div class="container d-flex justify-content-center">
		<div class="row">
			<div class="card">
				<div class="card-header">
					<strong>Login</strong>
				</div>
				<div class="card-body">
					<form name="login" id="login" method="post">
						<div class="row">
							<div class="col">
								<span class="profile-img">
									<i class='fas fa-user-circle' style='font-size:120px'></i>
								</span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<hr> <!-- other content  -->
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class='fas fa-user-shield'></i>
											</span>
										</div>
										<input class="form-control" placeholder="Email" id="loginEmail" name="loginEmail" type="email" autofocus>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class='fas fa-user-secret'></i>
											</span>
										</div>
										<input class="form-control" placeholder="Password" id="loginPassword" name="loginPassword" type="password">
									</div>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-lg btn-success btn-block submit" id="login_m" name="submit" value="Sign in">
								</div>
							</div>
						</div>
					<!-- <a href="#" onClick="">I forgot my password!</a> -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="dist/js/jquery-3.4.1.min.js"></script>
<script src="dist/bootstrap/js/bootstrap.min.js"></script>
<script src="dist/js/funciones.js"></script>
</body>
</html>
