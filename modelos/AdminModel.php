<?php
require_once "./config/Conexion.php";

/**
 * clase modelo para el administrador
 * @author iv&aacute;n cornejo <cornejo.quintana@hotmail.com>
 */
class AdminModel extends Conexion
{
    public function __construct() {
    }

    /**
     * Consulta de la lista de productos
     *
     * @return array $resultado
     */
    public function obtenerListaDeProductos()
    {
        $mysqli = $this->conectar();
        $resultado = $mysqli->query("SELECT * FROM Producto");
        $mysqli->close();
        return $resultado;
    }

    /**
     * funci&oacute;n que permite registra un nuevo producto
     *
     * @see https://www.php.net/manual/es/mysqli.prepare.php
     * @see https://www.php.net/manual/es/mysqli-stmt.execute.php
     *
     * @param array $array
     * @return void
     */
    public function guardarProductor($array = null)
    {
        if ($array == null)
        {
            return false;
        }
        else
        {
            // iniciamos la conexi&oacute;n
            $conexion = $this->conectar();

            // Prepara la consulta
            $stmt = $conexion->prepare("INSERT INTO Producto
            (Nombre, Precio, Cantidad, UnidadDeMedida, Descripcion, Imagen)
            VALUES (?, ?, ?, ?, ?, ?)");

            $stmt->bind_param("ssssss",
                    $Nombre,
                    $Precio,
                    $Cantidad,
                    $UnidadDeMedida,
                    $Descripcion,
                    $Imagen
                );

            $Nombre = $array['Nombre'];
            $Precio = $array['Precio'];
            $Cantidad = $array['Cantidad'];
            $UnidadDeMedida = $array['UnidadDeMedida'];
            $Descripcion = $array['Descripcion'];
            $Imagen = $array['Imagen'];

            if ($stmt->execute())
            { // Establecer parámetros y ejecutar (TOP)
                // Mensaje de éxito en la inserci&oacute;n
                echo "Se han registrado el producto exitosamente";
            } // Establecer parámetros y ejecutar (BOTTOM)
            else
            { // Mensaje de error en la inserci&oacute;n (TOP)
                echo "No han sido posible registrar el producto";
            } // Mensaje de error en la inserci&oacute;n (BOTTOM)

            // Cerrar conexiones
            $stmt->close();
            $conexion->close();
            return true;
        }
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function obtenerDetalleDelProducto($id = 0)
    {
        return null;
    }

    public function editarProducto(array $array = null, int $idProducto = 0)
    {
        if ($array == null || $idProducto==0)
        {
            return false;
        }
        else
        {
            // iniciamos la conexi&oacute;n
            $conexion = $this->conectar();

            // Prepara la consulta
            $stmt = $conexion->prepare("UPDATE Producto
            SET Nombre=?,
            Precio=?,Cantidad=?,
            UnidadDeMedida=?,Descripcion=?,Imagen=?
            WHERE idProducto = ?");

            $stmt->bind_param("sssssss",
                    $Nombre,
                    $Precio,
                    $Cantidad,
                    $UnidadDeMedida,
                    $Descripcion,
                    $Imagen,
                    $idProducto
                );

            $Nombre = $array['Nombre'];
            $Precio = $array['Precio'];
            $Cantidad = $array['Cantidad'];
            $UnidadDeMedida = $array['UnidadDeMedida'];
            $Descripcion = $array['Descripcion'];
            $Imagen = $array['Imagen'];

            if ($stmt->execute())
            { // Establecer parámetros y ejecutar (TOP)
                // Mensaje de éxito en la inserci&oacute;n
                echo "Se han registrado el producto exitosamente";
            } // Establecer parámetros y ejecutar (BOTTOM)
            else
            { // Mensaje de error en la inserci&oacute;n (TOP)
                echo "No han sido posible registrar el producto";
            } // Mensaje de error en la inserci&oacute;n (BOTTOM)

            // Cerrar conexiones
            $stmt->close();
            $conexion->close();
            return true;
        }
    }

    public function consultarUsuario($array = null)
    {

          // iniciamos la conexi&oacute;n
          $conexion = $this->conectar();

          // Prepara la consulta
          $stmt = $conexion->prepare("SELECT idUsuario, Nombre
            FROM Usuario WHERE Email = ? AND Password = ? AND Estatus = 1 ");

          $stmt->bind_param("ss",
                  $Email,
                  $Password
              );

          $Email = $array['Email'];
          $Password = $array['Password'];

          /**/
          $stmt->execute();

          /* obtener valor */
          $result = $stmt->get_result();

          // Cerrar conexiones
          $stmt->close();
          $conexion->close();

          return $result;
    }
}
