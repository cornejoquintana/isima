<?php
require_once "./modelos/CatalogoModel.php";

/**
 * Controlador que permite administrar los productos
 * @author  iv&aacute;n cornejo <cornejo.quintana@hotmail.com>
 */
class CatalogoControl
{

    /**
     * funci&oacute;n que permite obtener la lista de productos guardados en la base de datos
     *
     * @return void
     */
    public function listaDeProductos()
    {
        $catalogo = new CatalogoModel(); // instancia de la clase CatalogoModel
        return $catalogo->obtenerListaDeProductos(); // retorno del resultado que se realiza en el modelo
    }

    /**
     * funci&oacute;n que permite agregar un producto al carrito
     *
     * @param array $arrayPOST
     * @return void
     */
    public function aggregarAlCarrito($arrayPOST)
    {
        session_start();

        $carrito = array('id' => $arrayPOST['txtIdProducto'],
        'cantidad' => $arrayPOST['txtCantidad'],
        'otro' => array()
        );

        $_SESSION['carrito'] = $carrito;
    }

}
?>
