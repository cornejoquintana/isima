<?php
include 'vistas/header.php';

if (!isset($_SESSION["user"])) {
  header("Location: http://".$_SERVER['HTTP_HOST']);
}

if (isset($_POST["accion"])) {
    switch ($_POST["accion"]) {
        case 'editarProducto':
            $admin  = new AdminControl();
            $admin->editarProducto($_POST, $_FILES);
            break;
        case 'borrarProducto':
            # code...
            break;
        default:
            # code...
            break;
    }
}
?>

<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="NuevoProducto.php" method="post">
                    <input type="submit" value="Nuevo producto">
                </form>
            </div>
            <div class="col-md-12">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Unidad de medida</th>
                            <th scope="col">Descripci&oacute;n</th>
                            <th scope="col">Imagen</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $catalogo  = new AdminControl();
                        $productos = $catalogo->obtenerListaDeProductos();
                        $contador = 1;
                        foreach ($productos as $producto) :
                        ?>

                            <tr>
                                <th scope="row"><?php echo $contador; ?></th>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <td><input type="text" name="txtNombre" id="txtNombre" value="<?php echo $producto['Nombre']; ?>"></td>
                                    <td><input type="number" min="0" name="txtPrecio" id="txtPrecio" value="<?php echo $producto['Precio']; ?>"></td>
                                    <td><input type="number" min="0" name="txtCantidad" id="txtCantidad" value="<?php echo $producto['Cantidad']; ?>"></td>
                                    <td><input type="text" name="txtUnidadDeMedida" id="txtUnidadDeMedida" value="<?php echo $producto['UnidadDeMedida']; ?>"></td>
                                    <td><input type="text" name="txtDescripcion" id="txtDescripcion" value="<?php echo $producto['Descripcion']; ?>"></td>
                                    <td>
                                        <input type="text" disabled value="<?php echo $producto['Imagen']; ?>">
                                        <input type="file" class="form-control-file" name="imagen" id="imagen" accept="image/*">
                                    </td>
                                    <td>
                                        <input type="hidden" name="accion" value="editarProducto">
                                        <input type="hidden" name="txtIdProducto" id="txtIdProducto" value="<?php echo $producto['idProducto']; ?>">
                                        <input type="submit" value="Editar">
                                    </td>
                                    <td></td>
                                </form>
                            </tr>
                        <?php
                            $contador++;
                        endforeach;
                        ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<?php include 'vistas/footer.php'; ?>
