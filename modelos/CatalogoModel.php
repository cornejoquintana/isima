<?php
require_once "./config/Conexion.php";
/**
 * Modelo que permite manipular las operaciones en la base de datos para los productos
 */
class CatalogoModel extends Conexion
{

    /**
     * Consulta de la lista de productos
     *
     * @return array $resultado
     */
    public function obtenerListaDeProductos()
    {
        $mysqli = $this->conectar();
        $resultado = $mysqli->query("SELECT * FROM Producto");
        $mysqli->close();
        return $resultado;
    }

}

?>
