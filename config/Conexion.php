<?php
include 'Db.php';

/**
 * Clase que permite conectar a la base de datos
 * @author  iv&aacute;n cornejo <cornejo.quintana@hotmail.com>
 */
class Conexion
{
    /**
     * constructor 
     */
    public function __construct() {
        
    }

    /**
     * M&eacute;todo de conexi&oacuet;n
     *
     * @return mysql 
     */
    public function conectar()
    {
        $mysqli =  new mysqli(host, usuario, password, baseDeDatos);
        if ($mysqli->connect_errno) 
        { // si la conexi&oacute;n no se establece enviamos un mensaje informando el erro (TOP)
            echo "Fallo al conectar a MySQL: " . $mysqli->connect_error;
        } // si la conexi&oacute;n no se establece enviamos un mensaje informando el erro (BOTTOM)
        return $mysqli;
    }

}
?>