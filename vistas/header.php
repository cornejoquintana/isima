<?php
session_start();
require_once "./controladores/AdminControl.php";
require_once 'controladores/CatalogoControl.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Isima</title>
    <link rel="stylesheet" href="dist/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>
<body>
<?php
if (isset($_POST["accion"])) {
    switch ($_POST["accion"]) {
        case 'salir':
            $admin  = new AdminControl();
            $admin->cerrarSesion();
            break;
        default:
            # code...
            break;
    }
}
?>
<!-- menu (TOP)-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <ul class="navbar-nav mr-rhigth">
      <?php if (!isset($_SESSION['user'])): ?>
      <li class="nav-item">
        <a class="nav-link"  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/Login.php">Login</a>
      </li>
    <?php else: ?>
      <li class="nav-item">
        <a class="nav-link" onclick="frmLogout.submit()"  href="#">Salir</a>
      </li>
    <?php endif; ?>
    </ul>
  </div>
</nav>


<form name="frmLogout" method="post">
  <input type="hidden" name="accion" value="salir">
</form>
<!-- menu (BOTTOM)-->
