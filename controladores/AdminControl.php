<?php
require_once "./modelos/AdminModel.php";
/**
 * Controlador que permite
 */
class AdminControl
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * funci&oacute;n que permite crear los directorios para guardar archivos
     *
     * @see https://www.php.net/manual/es/function.mkdir
     * @see https://www.php.net/manual/es/function.file-exists
     *
     * @param string $ruta
     * @return boolean
     */
    public function crearCarpeta(string $ruta = null)
    {
        if ($ruta == null) { // si la ruta no esta definida (TOP)
            echo "Es necesario agregar una ruta!";
        } // si la ruta no esta definida (BOTTOM)
        else { // creando la ruta especificada (TOP)
            if (file_exists($ruta)) { // si la ruta existe devolvemos un valor verdadero (TOP)
                return true;
            } // si la ruta existe devolvemos un valor verdadero (BOTTOM)
            else { // si la ruta no existe creamos la estructura de directorios (TOP)
                if (!mkdir($ruta, 0755, true)) { // si hubo alg&uacute;n error al crear la ruta devolvemos un valor falso (TOP)
                    return false;
                } // si hubo alg&uacute;n error al crear la ruta devolvemos un valor falso (BOTTOM)
                else { // si se creo la ruta devolvemos un valor verdadero (TOP)
                    return true;
                } // si se creo la ruta devolvemos un valor verdadero (BOTTOM)
            } // si la ruta no existe creamos la estructura de directorios (BOTTOM)
        } // creando la ruta especificada (BOTTOM)
    }

    /**
     * permite registrar en la base de datos un nuevo producto
     *
     * @see https://www.php.net/manual/es/function.move-uploaded-file
     * @see https://www.php.net/manual/es/function.in-array
     * @see https://www.php.net/manual/es/function.date
     *
     * @return void
     */
    public function guardarProductor($postArray, $files)
    {
        if ($files['imagen']['name'] == null || empty($files['imagen']['name']))
        { // en caso de no haber ingresado una imagen (TOP)
            // asignaci&oacute;n de los valores
            $Nombre = $postArray['txtNombre'];
            $Precio = $postArray['txtPrecio'];
            $Cantidad = $postArray['txtCantidad'];
            $UnidadDeMedida = $postArray['txtUnidadDeMedida'];
            $Descripcion = $postArray['txtDescripcion'];

            // instancia del modelo
            $adminModel =  new AdminModel();

            // en este caso la inserci&oacute;n a la base de datos se usara una clase diferente a query
            // por lo que es posible enviar un arreglo para la consulta
            $arrayDeConsulta = array(
                'Nombre' => $Nombre,
                'Precio' => $Precio,
                'Cantidad' => $Cantidad,
                'UnidadDeMedida' => $UnidadDeMedida,
                'Descripcion' => $Descripcion,
                'Imagen' => null
            );

            if ($adminModel->guardarProductor($arrayDeConsulta)) { // si la inserci&oacute;n fue correcta avisamos al usuario (TOP)
                echo "Los datos se guardaron correctamente!";
            } // si la inserci&oacute;n fue correcta avisamos al usuario (BOTTOM)
            else { // aviso de error al no poder guardar los datos (TOP)
                echo "No fue posible guardar los datos! Intente de nuevo";
            } // aviso de error al no poder guardar los datos (BOTTOM)
        } // en caso de no haber ingresado una imagen (BOTTOM)
        else
        { // si se agrego una imagen  (TOP)
            if (!isset($files["imagen"]) || $files["imagen"]["error"] > 0) { // Comprobamos si ha ocurrido un error (TOP)
                echo "Ha ocurrido un error.";
            } // Comprobamos si ha ocurrido un error (BOTTOM)
            else { // si todo es correcto (TOP)
                // Verificamos si el tipo de archivo es un tipo de imagen permitido.
                $permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");

                // verificamos que el tama&ntilde;o del archivo no exceda los 16MB
                $limite_kb = 16384 * 1024;

                if (
                    in_array($files['imagen']['type'], $permitidos)
                    && $files['imagen']['size'] <= $limite_kb
                ) { // si el tipo de archivo y el tama&ntilde;o es el correcto guardamos la imagen (TOP)

                    // ruta para almacenar las imagenes
                    $rutaNormal = 'dist/img/productos/' . date("Y") . "/" . date("m");

                    $rutaDeCreacion = "./" . $rutaNormal;
                    if ($this->crearCarpeta($rutaDeCreacion)) { // si la ruta es correcta  guardamos los datos (TOP)

                        // Nombre temporal del archivo
                        $tmpNombre = $files["imagen"]["tmp_name"];

                        //basename — Devuelve el último componente de nombre de una ruta
                        $nombre = basename($files["imagen"]["name"]);

                        // Tipo de archivo
                        $tipo = $files['imagen']['type'];

                        //ruta completa del archivo
                        $rutaCompleta  = "$rutaNormal/$nombre";

                        if (move_uploaded_file($tmpNombre, $rutaDeCreacion . "/" . $nombre)) { // validamos si el archivo se guardo correctamente (TOP)

                            // asignaci&oacute;n de los valores
                            $Nombre = $postArray['txtNombre'];
                            $Precio = $postArray['txtPrecio'];
                            $Cantidad = $postArray['txtCantidad'];
                            $UnidadDeMedida = $postArray['txtUnidadDeMedida'];
                            $Descripcion = $postArray['txtDescripcion'];

                            // instancia del modelo
                            $adminModel =  new AdminModel();

                            // en este caso la inserci&oacute;n a la base de datos se usara una clase diferente a query
                            // por lo que es posible enviar un arreglo para la consulta
                            $arrayDeConsulta = array(
                                'Nombre' => $Nombre,
                                'Precio' => $Precio,
                                'Cantidad' => $Cantidad,
                                'UnidadDeMedida' => $UnidadDeMedida,
                                'Descripcion' => $Descripcion,
                                'Imagen' => $rutaCompleta
                            );

                            if ($adminModel->guardarProductor($arrayDeConsulta)) { // si la inserci&oacute;n fue correcta avisamos al usuario (TOP)
                                echo "Los datos se guardaron correctamente!";
                            } // si la inserci&oacute;n fue correcta avisamos al usuario (BOTTOM)
                            else { // aviso de error al no poder guardar los datos (TOP)
                                echo "No fue posible guardar los datos! Intente de nuevo";
                            } // aviso de error al no poder guardar los datos (TOP)
                        } // validamos si el archivo se guardo correctamente (BOTTOM)
                        else { // aviso de error al guardar la imagen (TOP)
                            echo "No fue posible guardar la imagen en el directorio $rutaDeCreacion";
                        } // aviso de error al guardar la imagen (BOTTOM)

                    } // si la ruta es correcta  guardamos los datos (BOTTOM)
                    else { // aviso de error al crear la ruta (TOP)
                        echo "No fue posible crear la ruta $ruta";
                    } // aviso de error al crear la ruta (BOTTOM)
                } // si el tipo de archivo y el tama&ntilde;o es el correcto guardamos la imagen (BOTTOM)
                else { // aviso en caso de no cumplir con el tipo y el tama&ntilde;o permitido (TOP)
                    echo "Formato de archivo no permitido o excede el tama&ntilde;o l&iacute;mite de $limite_kb Kbytes.";
                } // aviso en caso de no cumplir con el tipo y el tama&ntilde;o permitido (BOTTOM)
            } // si todo es correcto (BOTTOM)
        } // si se agrego una imagen  (BOTTOM)
    }

    /**
     * Obtiene la lista de productos para el administrador
     *
     * @return array
     */
    public function obtenerListaDeProductos()
    {
        $catalogo = new AdminModel(); // instancia de la clase CatalogoModel
        return $catalogo->obtenerListaDeProductos(); // retorno del resultado que se realiza en el modelo
    }

    /**
     * Obtener el detalle de un producto en general
     *
     * @param integer $id
     * @return void
     */
    public function obtenerDetalleDelProducto(int $id =  0)
    {
        $catalogo = new AdminModel(); // instancia de la clase CatalogoModel
        return $catalogo->obtenerDetalleDelProducto($id); // retorno del resultado que se realiza en el modelo
    }

    /**
     * Permite editar los datos de un producto
     *
     * @param array $postArray
     * @param array $files
     * @return void
     */
    public function editarProducto($postArray, $files)
    {
        if ($files['imagen']['name'] == null || empty($files['imagen']['name']))
        { // en caso de no tener imagen (TOP)
            // asignaci&oacute;n de los valores
            $Nombre = $postArray['txtNombre'];
            $Precio = $postArray['txtPrecio'];
            $Cantidad = $postArray['txtCantidad'];
            $UnidadDeMedida = $postArray['txtUnidadDeMedida'];
            $Descripcion = $postArray['txtDescripcion'];
            $idProducto = $postArray['txtIdProducto'];

            // instancia del modelo
            $adminModel =  new AdminModel();

            // en este caso la inserci&oacute;n a la base de datos se usara una clase diferente a query
            // por lo que es posible enviar un arreglo para la consulta
            $arrayDeConsulta = array(
                'Nombre' => $Nombre,
                'Precio' => $Precio,
                'Cantidad' => $Cantidad,
                'UnidadDeMedida' => $UnidadDeMedida,
                'Descripcion' => $Descripcion,
                'Imagen' => null
            );

            if ($adminModel->editarProducto($arrayDeConsulta, $idProducto)) { // si la inserci&oacute;n fue correcta avisamos al usuario (TOP)
                echo "Los datos se guardaron correctamente!";
            } // si la inserci&oacute;n fue correcta avisamos al usuario (BOTTOM)
            else { // aviso de error al no poder guardar los datos (TOP)
                echo "No fue posible guardar los datos! Intente de nuevo";
            } // aviso de error al no poder guardar los datos (TOP)
        } // en caso de no tener imagen (BOTTOM)
        else
        { // si se agrego una imagen  (TOP)
            if (!isset($files["imagen"]) || $files["imagen"]["error"] > 0) { // Comprobamos si ha ocurrido un error (TOP)
                echo "Ha ocurrido un error.";
            } // Comprobamos si ha ocurrido un error (BOTTOM)
            else
            { // si todo es correcto (TOP)
                // Verificamos si el tipo de archivo es un tipo de imagen permitido.
                $permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");

                // verificamos que el tama&ntilde;o del archivo no exceda los 16MB
                $limite_kb = 16384 * 1024;

                if (
                    in_array($files['imagen']['type'], $permitidos)
                    && $files['imagen']['size'] <= $limite_kb
                ) { // si el tipo de archivo y el tama&ntilde;o es el correcto guardamos la imagen (TOP)

                    // ruta para almacenar las imagenes
                    $rutaNormal = 'dist/img/productos/' . date("Y") . "/" . date("m");

                    $rutaDeCreacion = "./" . $rutaNormal;
                    if ($this->crearCarpeta($rutaDeCreacion)) { // si la ruta es correcta  guardamos los datos (TOP)

                        // Nombre temporal del archivo
                        $tmpNombre = $files["imagen"]["tmp_name"];

                        //basename — Devuelve el último componente de nombre de una ruta
                        $nombre = basename($files["imagen"]["name"]);

                        // Tipo de archivo
                        $tipo = $files['imagen']['type'];

                        //ruta completa del archivo
                        $rutaCompleta  = "$rutaDeCreacion/$nombre";

                        if (move_uploaded_file($tmpNombre, $rutaCompleta)) { // validamos si el archivo se guardo correctamente (TOP)

                            // asignaci&oacute;n de los valores
                            $Nombre = $postArray['txtNombre'];
                            $Precio = $postArray['txtPrecio'];
                            $Cantidad = $postArray['txtCantidad'];
                            $UnidadDeMedida = $postArray['txtUnidadDeMedida'];
                            $Descripcion = $postArray['txtDescripcion'];
                            $idProducto = $postArray['txtIdProducto'];

                            // instancia del modelo
                            $adminModel =  new AdminModel();

                            // en este caso la inserci&oacute;n a la base de datos se usara una clase diferente a query
                            // por lo que es posible enviar un arreglo para la consulta
                            $arrayDeConsulta = array(
                                'Nombre' => $Nombre,
                                'Precio' => $Precio,
                                'Cantidad' => $Cantidad,
                                'UnidadDeMedida' => $UnidadDeMedida,
                                'Descripcion' => $Descripcion,
                                'Imagen' => $rutaCompleta
                            );

                            if ($adminModel->editarProducto($arrayDeConsulta, $idProducto)) { // si la inserci&oacute;n fue correcta avisamos al usuario (TOP)
                                echo "Los datos se guardaron correctamente!";
                            } // si la inserci&oacute;n fue correcta avisamos al usuario (BOTTOM)
                            else { // aviso de error al no poder guardar los datos (TOP)
                                echo "No fue posible guardar los datos! Intente de nuevo";
                            } // aviso de error al no poder guardar los datos (TOP)
                        } // validamos si el archivo se guardo correctamente (BOTTOM)
                        else { // aviso de error al guardar la imagen (TOP)
                            echo "No fue posible guardar la imagen en el directorio $rutaDeCreacion";
                        } // aviso de error al guardar la imagen (BOTTOM)

                    } // si la ruta es correcta  guardamos los datos (BOTTOM)
                    else { // aviso de error al crear la ruta (TOP)
                        echo "No fue posible crear la ruta $rutaNormal";
                    } // aviso de error al crear la ruta (BOTTOM)
                } // si el tipo de archivo y el tama&ntilde;o es el correcto guardamos la imagen (BOTTOM)
                else { // aviso en caso de no cumplir con el tipo y el tama&ntilde;o permitido (TOP)
                    echo "Formato de archivo no permitido o excede el tama&ntilde;o l&iacute;mite de $limite_kb Kbytes.";
                } // aviso en caso de no cumplir con el tipo y el tama&ntilde;o permitido (BOTTOM)
            } // si todo es correcto (BOTTOM)
        } // si se agrego una imagen  (BOTTOM)
    }

    public function login($email = null, $password =  null)
    {
      // instancia del modelo
      $adminModel =  new AdminModel();

      // por lo que es posible enviar un arreglo para la consulta
      $arrayDeConsulta = array(
          'Email' => $email,
          'Password' => $password
      );

      $result = $adminModel->consultarUsuario($arrayDeConsulta);

      if ($result != null) {
        session_start();
        $Id = null;
        $Nombre = null;
        foreach ($user as $result) {
          $Id = $user['idUsuario'];
          $Nombre = $user['Nombre'];
        }
        $_SESSION["user"] = array('userId' => $Id, 'userName' => $Nombre);
        echo "Ok...

        ";
      }
      else {
        echo "Intenta de nuevo!";
      }
    }

    public function cerrarSesion()
    {
      $_SESSION = array();
      session_destroy();
    }
}
