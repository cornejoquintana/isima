<?php
require_once 'vistas/header.php';

if (isset($_POST["submit"])) { // validamos si se ha enviado el m&eacute;todo $_POST (TOP)
    $admin  = new AdminControl();
    $admin->guardarProductor($_POST, $_FILES);
} // validamos si se ha enviado el m&eacute;todo $_POST (BOTTOM)
if (!isset($_SESSION["user"])) {
  header("Location: http://".$_SERVER['HTTP_HOST']);
}
?>

    <div class="container py-1">
        <div class="row">
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="txtNombre">Nombre</label>
                <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Nombre del producto">
            </div>
            <div class="form-group">
                <label for="txtPrecio">Precio</label>
                <input type="number" min="0" class="form-control" name="txtPrecio" id="txtPrecio" placeholder="$">
            </div>
            <div class="form-group">
                <label for="txtCantidad">Cantidad</label>
                <input type="number"  min="0" class="form-control" name="txtCantidad" id="txtCantidad" min="0" placeholder="12">
            </div>
            <div class="form-group">
                <label for="txtUnidadDeMedida">Unidad de medida</label>
                <input type="text" class="form-control" name="txtUnidadDeMedida" id="txtUnidadDeMedida" placeholder="pzs">
            </div>
            <div class="form-group">
                <label for="txtDescripcion">Descripci&oacute;n</label>
                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="txtImagen">Imagen</label>
                <input type="file" class="form-control-file" name="txtImagen" id="txtImagen" accept="image/*">
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
    </div>

<?php include 'vistas/footer.php'; ?>
