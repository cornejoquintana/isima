<?php
include 'vistas/header.php';
?>
<!-- Contenido (TOP) -->
<div class="container py-3">
    <!-- Carousel (TOP) -->

    <!-- Carousel (BOTTOM) -->

    <div class="row">
    <?php
      $catalogo  = new CatalogoControl();
      $productos = $catalogo->listaDeProductos();
      foreach ($productos as $producto):
    ?>
        <div class="col-sm-4 py-0">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top rounded-sm" src="<?php echo $producto['Imagen']; ?>" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title"><?php echo $producto['Nombre']; ?></h5>
              <p class="card-text">
              <?php echo $producto['Descripcion']; ?>
              </p>
              <form action="" method="post">
                <input type="hidden" name="idProducto" value="<?php echo $producto['idProducto']; ?>">
                <input type="submit" class="btn btn-success" value="Ver mas">
              </form>
          </div>
        </div>
      </div>
    <?php
      endforeach;
    ?>
    </div>

</div>
<!-- Contenido (BOTTOM) -->

<?php include 'vistas/footer.php'; ?>
