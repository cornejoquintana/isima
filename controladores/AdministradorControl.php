<?php
class AdministradorControl
{

    public function crearCarpeta(string $ruta = null)
    {

        if ($ruta == null)
        {
            echo "Se necesita una ruta!";
        }
        else
        {
            if (file_exists($ruta))
            {
                return true;
            }
            else
            {
                if (!mkdir($ruta, 0755, true))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

        }
    } 

}
?>